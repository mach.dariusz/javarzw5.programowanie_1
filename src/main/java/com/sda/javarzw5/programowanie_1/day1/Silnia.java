package com.sda.javarzw5.programowanie_1.day1;

public class Silnia {
    public static int calculate(int liczba) {

        if (liczba < 0) {
            // throw new RuntimeException("Silnia moze byc liczona tylko dla wartosci wiekszych lub rownych 0");
            throw new WrongNumberException("Silnia moze byc liczona tylko dla wartosci wiekszych lub rownych 0");
        }

        if (liczba == 0) {
            return 1;
        } else {
            return liczba * calculate(liczba - 1);
        }
    }
}


class WrongNumberException extends RuntimeException {
    public WrongNumberException(String message) {
        super(message);
    }
}