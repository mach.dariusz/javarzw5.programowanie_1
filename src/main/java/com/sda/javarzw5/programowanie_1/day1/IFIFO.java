package com.sda.javarzw5.programowanie_1.day1;

public interface IFIFO<E> {

    boolean push(E e);

    E pop();

    E peek();
}
