package com.sda.javarzw5.programowanie_1.day1;

public class NWD {

    public static int calculate(int a, int b) {

        while (a != b) {
            if (a < b) {
                b = b - a;
            } else {
                a = a - b;
            }
        }

        return a;
    }
}
