package com.sda.javarzw5.programowanie_1.day1;

public class App {


    public static void main(String[] args) {
        System.out.println("Hello World!");

        MyCustomLIFO<String> stack = new MyCustomLIFO(3);
        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");
        stack.print(); // [A, B, C]
        stack.pop();
        System.out.println(stack.peek()); // B
        stack.pop();
        stack.print(); // [A]


        MyCustomFIFO<Integer> queue = new MyCustomFIFO<>(5);

        for (int i = 0; i < 100; i++) {

            if (queue.isFull()) {
                System.out.println("Kolejka jest pelna, sciagam: " + queue.pop());
            }
            queue.push(i);

        }

        queue.print();
    }
}


