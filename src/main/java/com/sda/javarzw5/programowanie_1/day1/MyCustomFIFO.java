package com.sda.javarzw5.programowanie_1.day1;

import java.util.Arrays;

public class MyCustomFIFO<E> implements IFIFO<E> {

    private E[] queue;
    private int head;
    private int tail;
    private int size;
    private int capacity;

    public MyCustomFIFO(int capacity) {
        this.capacity = capacity;
        this.queue = (E[]) new Object[capacity];
        this.head = 0;
        this.tail = 0;
        this.size = 0;
    }

    @Override
    public boolean push(E e) {
        if (!isFull()) {
            this.queue[this.head] = e;
            this.head = (this.head + 1) % this.capacity;
            this.size++;

            return true;
        } else {
            return false;
        }
    }

    @Override
    public E pop() {
        if (!isEmpty()) {
            E returnObject = this.queue[this.tail];
            this.queue[this.tail] = null;
            this.tail = (this.tail + 1) % this.capacity;
            this.size--;

            return returnObject;
        } else {
            return null;
        }
    }

    @Override
    public E peek() {
        if (!isEmpty()) {
            return this.queue[this.tail];
        } else {
            return null;
        }
    }

    public boolean isFull() {
        return this.size == this.capacity;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public int getSize() {
        return this.size;
    }

    public void print() {
        System.out.println(Arrays.toString(this.queue));
        System.out.println("Queue size: " + getSize());
    }

}
