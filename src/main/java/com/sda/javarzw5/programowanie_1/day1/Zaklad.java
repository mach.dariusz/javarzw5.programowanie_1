package com.sda.javarzw5.programowanie_1.day1;

public class Zaklad {

    public static void main(String[] args) {
        System.out.println(calculateLog2N(1_000_000));
        System.out.println(turns(250_000, 1, 1_000_000));
    }

    public static int calculateLog2N(int n) {
        double result = Math.round(Math.log10(n) / Math.log10(2));
        return (int) result;
    }

    // Wyznacza liczbę kroków, gdy n na pewno znajduje się w przedziale
    // [dolne, górne] ([low, high]).
    // example turns(500_000, 1, 1_000_000)
    public static int turns(int szukana_liczba, int poczatek_zbioru, int koniec_zbioru) {
        int turns = 0;

        // Wykonuj, dopóki do sprawdzenia pozostają więcej niż 2 liczby.
        while (koniec_zbioru - poczatek_zbioru >= 2) {
            // Przygotuj punkt środkowy [low, high] jako wybór.
            turns++;
            int mid = (poczatek_zbioru + koniec_zbioru) / 2;
            if (mid == szukana_liczba) {
                return turns;
            } else if (mid < szukana_liczba) {
                poczatek_zbioru = mid + 1;
            } else {
                koniec_zbioru = mid - 1;
            }
        }
        // Tutaj zostały już tylko dwie liczby. Wybieramy jedną z nich i jeśli to
        // nie ta, to odgadywaną jest druga. Dochodzi więc tylko jeden krok.
        return turns + 1;
    }
}
