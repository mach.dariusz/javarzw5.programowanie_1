package com.sda.javarzw5.programowanie_1.day1;

import java.util.Arrays;

public class MyCustomLIFO<T> {

    private T[] stack;
    private int head;
    private int size;
    private int capacity;

    public MyCustomLIFO(int capacity) {
        this.capacity = capacity;
        this.stack = (T[]) new Object[capacity];
        this.head = 0;
        this.size = 0;
    }


    // dodawanie do stosu
    public boolean push(T object) {
        if (!isFull()) {
            this.stack[this.head] = object;
            this.head++;
            this.size++;

            return true;
        } else {
            return false;
        }
    }

    // sciaganie ze stosu - z wierzcholka
    public T pop() {
        if (!isEmpty()) {
            T returnObject = this.stack[this.head - 1];
            this.stack[this.head - 1] = null;
            this.head--;
            this.size--;

            return returnObject;
        } else {
            return null;
        }
    }

    // podgladanie wierzcholka - bez usuwania obiektu
    public T peek() {
        if (!isEmpty()) {
            return this.stack[this.head - 1];
        } else {
            return null;
        }
    }

    public boolean isFull() {
        return this.size == this.capacity;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public int getSize() {
        return this.size;
    }

    public void print() {
        System.out.println(Arrays.toString(this.stack));
        System.out.println("Stack size: " + getSize());
    }

}
