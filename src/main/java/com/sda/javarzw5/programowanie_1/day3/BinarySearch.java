package com.sda.javarzw5.programowanie_1.day3;

public class BinarySearch {

    /**
     * return -1 if not found or index
     */
    static int searchRecursive(int number, int[] array, int low, int high) {

        // wyznaczamy mid - dzielimy na pol
        int middle = (low + high) / 2;

        // warunek na zakonczenie rekursji - jesli nie znajdziemy szukanego obiektu
        if (high < low) {
            return -1;
        }

        // sprawdzamy rownosc, mniejszosc lub wiekszosc
        if (number == array[middle]) {
            return middle;
        } else if (number < array[middle]) {
            return searchRecursive(number, array, low, middle - 1);
        } else {
            return searchRecursive(number, array, middle + 1, high);
        }

    }

    /**
     * return -1 if not found or index
     */
    static int searchIterative(int number, int[] array, int low, int high) {

        // petla while
        while (low <= high) {

            // wyznaczenie mid
            int middle = (low + high) / 2;

            // sprawdzenie czy rowny, mniejszy, wiekszy
            if (number == array[middle]) {
                return middle;
            } else if (number < array[middle]) {
                high = middle - 1;
            } else {
                low = middle + 1;
            }
        }

        // zwrocenie indexu
        return -1;
    }

}
