package com.sda.javarzw5.programowanie_1.day3;

import java.util.Arrays;
import java.util.Collections;

public class App {

    static int[] sortedArray = {-10, -8, -3, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 20, 30, 40, 51, 52, 53};

    static int[] unsortedArray = {-1, 12, 8, 9, -10, -8, -3, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

    public static void main(String[] args) {
        System.out.println(Arrays.binarySearch(sortedArray, 1));

        // nie dziala dla nieposortowanej tablicy
        System.out.println(Arrays.binarySearch(unsortedArray, 12));
    }
}