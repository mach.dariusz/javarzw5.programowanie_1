package com.sda.javarzw5.programowanie_1.day3;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

class Solution {

    /**
     * N is an odd integer within the range [1..1,000,000];
     * each element of array A is an integer within the range [1..1,000,000,000];
     * all but one of the values in A occur an even number of times.
     */
    static public int solution_1(int[] A) {
        int result = 0;

        System.out.println("starting result: " + result);
        for (int i = 0; i < A.length; i++) {
            System.out.printf("%d XOR %d | %4s XOR %4s | = %d %n", result, A[i], Integer.toBinaryString(result), Integer.toBinaryString(A[i]), result ^ A[i]);
            result = result ^ A[i];
        }
        System.out.println("final result: " + result);
        return result;

    }

    static public int solution_2(int[] A) throws Exception {
        int counter = 0;
        int previousNumber = 0;

        Arrays.sort(A);
        for (int currentNumber : A) {
            if (currentNumber != previousNumber) {
                if (counter != 0 && counter % 2 != 0) {
                    return previousNumber;
                }
                previousNumber = currentNumber;
                counter = 1;
            } else {
                counter++;
            }
        }

        if (counter % 2 != 0) {
            return previousNumber;
        } else {
            throw new Exception("Wrong array passed to the method!");
        }
    }

    static public int solution_3(int[] A) throws Exception {
        /*
        if ("Darek" == "Darek") {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

        System.out.println("Darek" == "Darek" ? "Super Darek" : "Niedobry Darek");

        int wartosc = A.length == 10 ? 1000 : -1;
        */


        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            map.put(A[i], map.containsKey(A[i]) ? map.get(A[i]) + 1 : 1);
        }

        for (Integer key: map.keySet()) {
            if (map.get(key) % 2 != 0) {
                return key;
            }
        }

        throw new Exception("Wrong array passed to the method!");

    }


}