package com.sda.javarzw5.programowanie_1.day3;

public class WyszukiwanieLiniowe {


    int[] array = {1, 24, 2, 15, 3, 10, 4, 1, 4};

    // dla 24, return 1
    // dla 10, return 5
    // dla 4, return 6
    // dla 21, return -1


    // return index
    // return -1 - jesli szukany obiekt nie znajduje sie w tablicy
    static int znajdzLiczbe(int liczba, int[] array) {

        for (int i = 0; i < array.length; i++) {
            if (array[i] == liczba) {
                return i;
            }
        }

        return -1;
    }




}
