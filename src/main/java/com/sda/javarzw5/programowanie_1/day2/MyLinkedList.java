package com.sda.javarzw5.programowanie_1.day2;

public class MyLinkedList<T> {

    private Node head = null;

    MyLinkedList() {
        System.out.println("Initial state: " + this.head);
    }

    public void insert(T data) {
        Node node = new Node(data);
        node.setNext(this.head);
        this.head = node;
        System.out.println("Dodano: " + node);
    }
}

class Node<T> {
    private T data;    // infromacja jaki obiekt przechowuje
    private Node next;      // informacje o poprzednim obiekcie

    public Node(T data) {
        this.data = data;
        this.next = null;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    @Override
    public String toString() {
        String result = "data: " + this.data;

        if (this.next != null) {
            result += " | next: " + this.next.data;
        } else {
            result += " | next: null";
        }

        return result;
    }
}
