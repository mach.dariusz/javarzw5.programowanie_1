package com.sda.javarzw5.programowanie_1.day2.sorting;

import java.util.logging.Logger;

public class Logowanie {

    final static Logger LOG = Logger.getLogger(Logowanie.class.getName());

    public static void main(String[] args) {

        System.out.println(Logowanie.class.getName());
        LOG.info("jakas wiadomosc na poziomie info");
        LOG.warning("jakis warning");
        LOG.finest("finiest warning");

        LOG.severe("");
        LOG.config("");

        // Log4J
        // SLF4J
        // Apache Common Logging

    }
}
