package com.sda.javarzw5.programowanie_1.day2;

import java.util.Stack;

public class ONP {

    private static double result;
    private static final Stack<Double> STACK = new Stack<>();

    /*
     * Krok 1:	Czytaj element z wyrazenia ONP
     * Krok 2:	Jeśli element nie jest liczbą, to idź do kroku 5
     * Krok 3:	Umieść element na stosie
     * Krok 4:	Idź do kroku 1
     * Krok 5:	Jeśli element jest znakiem '=', to idź do kroku 10
     * Krok 6:	Pobierz ze stosu dwie liczby a i b
     * Krok 7:	Wykonaj nad liczbami a i b operację określoną przez element i umieść wynik w zmiennej result
     * Krok 8:	Umieść result na stosie
     * Krok 9:	Idź do kroku 1
     * Krok 10:	Prześlij na wyjście zawartość wierzchołka stosu
     * Krok 11:	Zakończ
     */
    public static double calculate(String ONP_expression) {
        char[] elements = ONP_expression.toCharArray();

        for (int i = 0; i < elements.length; i++) {
            // check if character is not a digit
            if (!Character.isDigit(elements[i])) {

                // check if is equals sign
                if (isEqualSign(elements[i])) {
                    return STACK.pop();
                }

                result = calculateTwoNumbers(STACK.pop(), STACK.pop(), elements[i]);
                putElementInStack(result);
            } else {
                // it's a number
                putElementInStack(Character.digit(elements[i], 10));
            }

        }

        return 0;
    }


    private static boolean isEqualSign(char element) {
        if (element == '=') {
            return true;
        } else {
            return false;
        }
    }

    private static void putElementInStack(double number) {
        STACK.push(number);
    }

    private static double calculateTwoNumbers(Double number1, Double number2, char operation) {
        switch (operation) {
            case '+':
                return number2 + number1;
            case '-':
                return number2 - number1;
            case '*':
                return number2 * number1;
            case '/':
                return number2 / number1;
            case '^':
                return Math.pow(number2, number1);
        }

        return 0;
    }

}
