package com.sda.javarzw5.programowanie_1.day2.streams;


import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {


        List<Employee> list = Arrays.asList(
                new Employee("Jan Kowalski", 20),
                new Employee("Adam Jakis", 18),
                new Employee("Danuta Szafarska", 25),
                new Employee("Bogdan Ze Wsi", 30),
                new Employee("Janusz Od Haliny", 45)
        );


        list.stream().filter(employee -> employee.age == 36).findAny().orElse(null);


        list.stream().filter(emp -> emp.age > 40).forEach(System.out::println);

        list.stream()
                .filter(employee -> employee.age > 29)
                .map(employee -> {
                    employee.name += "!!!!";
                    return employee;
                })
                .forEach(System.out::println);


        List lista2 = list.stream().filter(employee -> employee.age >= 25).collect(Collectors.toList());
        System.out.println(lista2);



    }

}

class Employee {
    String name;
    int age;

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return this.name + " | " + this.age;
    }
}

class CustomHuman {
    int salary;

    public CustomHuman(int age) {
        this.salary = 1000 * age;
    }


    @Override
    public String toString() {
        return "" + this.salary;
    }
}