package com.sda.javarzw5.programowanie_1.day2.sorting;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        int[] array = {4, 2, 5, 1, 7};

        System.out.println("before sorting: " + Arrays.toString(array));
        sort(array);
        System.out.println("after sorting: " + Arrays.toString(array));
    }

    public static void sort(int[] array) {

        int n = array.length;
        int temp = 0;

        for (int i = 0; i < n; i++) {
            // System.out.println();
            // System.out.printf(" ************ Turn: %d ************ %n", i + 1);

            for (int j = 1; j < n - i; j++) {
                // System.out.printf("array[%d] > array[%d] -> %d > %d %n", j-1, j, array[j - 1], array[j]);

                if (array[j -1] > array[j]) {
                    // System.out.printf("-------> swap %d with %d %n", array[j - 1], array[j]);

                    // swap items
                    temp = array[j -1];
                    array[j -1] = array[j];
                    array[j] = temp;
                }
            }

            // System.out.printf("After %d -> %s %n", i + 1, Arrays.toString(array));
        }
    }
}
