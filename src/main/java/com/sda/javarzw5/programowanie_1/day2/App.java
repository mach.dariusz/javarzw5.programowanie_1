package com.sda.javarzw5.programowanie_1.day2;

public class App {

    public static void main(String[] args) {

        MyLinkedList listaWiazana = new MyLinkedList();

        // lista jest pusta - czyli head = null

        // dodajemy A
        listaWiazana.insert("A");
        // Node {
        //      data: "A"
        //      next: null // ustawiony na podstawie head
        // }
        // head = Node { data: "A" next: null }

        // dodajemy B
        listaWiazana.insert("B");
        // Node {
        //      data: "B"
        //      next: Node { data: "A" next: null } // ustawiony na podstawie head
        // }
        // head = Node { data: "B" next: Node { data: "A" next: null } }

        // dodajemy C
        listaWiazana.insert("C");
        // Node {
        //      data: "C"
        //      next: Node { data: "B" next: next: Node { data: "A" next: null } } // ustawiony na podstawie head
        // }
        // head = Node { data: "C" next: Node { data: "B" next: next: Node { data: "A" next: null } }


    }
}
