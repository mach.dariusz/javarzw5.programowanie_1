package com.sda.javarzw5.programowanie_1.day2.sorting;

import java.lang.reflect.Array;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Random;

public class QuickSort {

    public static void main(String[] args) {
        int[] array = {9, -3, 5, 2, 6, 8, -6, 1, 3};
        System.out.println("before sorting: " + Arrays.toString(array));
        quickSort(array, 0, array.length - 1);
        System.out.println("after sorting: " + Arrays.toString(array));


        int[] randomArray = createArray(200_000);
        System.out.println("before sort: "  + Arrays.toString(randomArray));

        // bs
        Instant start = Instant.now();
        // BubbleSort.sort(randomArray);
        Instant end = Instant.now();
        System.out.println("after sort: " + Arrays.toString(randomArray));
        long elapsedTime = Duration.between(start, end).toMillis();
        System.out.printf("Time elapsed for sort: %s milliseconds%n", elapsedTime);

        // qs
        start = Instant.now();
        quickSort(randomArray, 0, randomArray.length - 1);
        end = Instant.now();
        System.out.println("after sort: " + Arrays.toString(randomArray));
        elapsedTime = Duration.between(start, end).toMillis();
        System.out.printf("Time elapsed for sort: %s milliseconds%n", elapsedTime);

    }

    public static void quickSort(int[] array, int begin, int end) {

        if (begin < end) {

            int partitionIndex = partition(array, begin, end);

            quickSort(array, begin, partitionIndex - 1);
            quickSort(array, partitionIndex + 1, end);

        }

    }

    private static int partition(int[] array, int begin, int end) {
        int pivot = array[end];
        // System.out.println("pivot: " + pivot);

        int i = begin - 1;

        for (int j = begin; j < end; j++) {
            if (array[j] <= pivot) {
                i++;

                // swap items on left side
                int swapTemp = array[i];
                array[i] = array[j];
                array[j] = swapTemp;
            }
        }

        // swap elements on the right from the pivot
        int swapTemp = array[i + 1];
        array[i + 1] = array[end];
        array[end] = swapTemp;

        return i + 1;
    }


    private static int[] createArray(int capacity) {
        int[] array = new int[capacity];
        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(capacity);
        }

        return array;
    }
}
