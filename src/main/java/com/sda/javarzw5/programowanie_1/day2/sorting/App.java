package com.sda.javarzw5.programowanie_1.day2.sorting;


import java.util.Arrays;
import java.util.Comparator;

public class App {


    public static void main(String[] args) {

        comapreByAge();
        comapreByAgeDESC();
        comapreByLastName();
        comapreByLastCharOfLastName();
    }

    private static void comapreByLastCharOfLastName() {
        Employee[] array1 = getEmployees();

        // Arrays.stream(array1) -> tak mozna dostac stream z tablicy

        System.out.println("\nBefore sorting: ");
        for (Employee e: array1) {
            System.out.println(e);
        }

        Arrays.sort(array1, (e1, e2) -> e1.getLastName().charAt(e1.getLastName().length() - 1) - e2.getLastName().charAt(e2.getLastName().length() - 1));

        System.out.println("\nAfter sorting: ");
        for (Employee e: array1) {
            System.out.println(e);
        }

    }

    private static void comapreByLastName() {
        Employee[] array1 = getEmployees();
        System.out.println("\nBefore sorting: ");
        for (Employee e: array1) {
            System.out.println(e);
        }

        Arrays.sort(array1, (e1, e2) -> e2.getLastName().compareTo(e1.getLastName()));

        System.out.println("\nAfter sorting: ");
        for (Employee e: array1) {
            System.out.println(e);
        }

    }

    private static void comapreByAgeDESC() {
        Employee[] array1 = getEmployees();
        System.out.println("\nBefore sorting: ");
        for (Employee e: array1) {
            System.out.println(e);
        }

        Arrays.sort(array1, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o2.getAge() - o1.getAge();
            }
        });

        Arrays.sort(array1, (o1, o2) -> o2.getAge() - o1.getAge());

        System.out.println("\nAfter sorting: ");
        for (Employee e: array1) {
            System.out.println(e);
        }
    }


    private static void comapreByAge() {
        Employee[] array1 = getEmployees();
        System.out.println("\nBefore sorting: ");
        for (Employee e: array1) {
            System.out.println(e);
        }

        Arrays.sort(array1, new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.getAge() - o2.getAge();
            }
        });

        Arrays.sort(array1, (o1, o2) -> o1.getAge() - o2.getAge());

        Arrays.sort(array1, Comparator.comparingInt(Employee::getAge));

        Arrays.sort(array1, Comparator.comparingInt(employee -> employee.getAge()));

        System.out.println("\nAfter sorting: ");
        for (Employee e: array1) {
            System.out.println(e);
        }
    }

    private static Employee[] getEmployees() {
        Employee[] array = {
                new Employee("Dariusz", "Mach", 32),
                new Employee("Bartosz", "Kurek", 34),
                new Employee("Kuba", "Blaszczykowski", 35),
                new Employee("Katarzyna", "Kowalska", 20),
                new Employee("Jan", "Kowalski", 28),
                new Employee("John", "Wick", 38),
        };

        return array;
    }

}


class Employee {
    private String firstName;
    private String lastName;
    private int age;

    public Employee(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}
