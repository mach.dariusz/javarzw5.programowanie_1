package com.sda.javarzw5.programowanie_1.day2;

import java.math.BigDecimal;

public class App2 {

    public static void main(String[] args) {
        double a = 0.1;
        double b = 0.2;

        System.out.println(a + b);


        BigDecimal number1 = new BigDecimal("0.1");
        BigDecimal number2 = new BigDecimal("0.2");

        System.out.println(number1.add(number2));
    }
}
