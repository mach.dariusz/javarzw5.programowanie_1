package com.sda.javarzw5.programowanie_1.day3;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    int[] A = {9, 3, 9, 3, 9, 7, 9};

    @Test
    public void solution_1() {
        Assert.assertEquals(Solution.solution_1(A), 7);
    }

    @Test
    public void solution_2() throws Exception {
        Assert.assertEquals(Solution.solution_2(A), 7);
    }

    @Test
    public void solution_3() throws Exception {
        Assert.assertEquals(Solution.solution_3(A), 7);
    }
}