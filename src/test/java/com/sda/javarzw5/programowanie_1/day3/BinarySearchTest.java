package com.sda.javarzw5.programowanie_1.day3;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTest {

    int[] array = {-10, -8, -3, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 20, 30, 40, 51, 52, 53};

    @Test
    public void searchRecursive() {
        Assert.assertEquals(
                BinarySearch.searchRecursive(-9, array, 0, array.length - 1),
                -1);
        Assert.assertEquals(
                BinarySearch.searchRecursive(52, array, 0, array.length - 1),
                21);
        Assert.assertEquals(
                BinarySearch.searchRecursive(-10, array, 0, array.length - 1),
                0);
    }

    @Test
    public void searchIterative() {
        Assert.assertEquals(
                BinarySearch.searchIterative(-9, array, 0, array.length - 1),
                -1);
        Assert.assertEquals(
                BinarySearch.searchIterative(52, array, 0, array.length - 1),
                21);
    }
}