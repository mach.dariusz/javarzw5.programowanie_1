package com.sda.javarzw5.programowanie_1.day3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WyszukiwanieLinioweTest {

    int[] array = {1, 24, 2, 15, 3, 10, 4, 1, 4};

    @Test
    public void znajdzLiczbe_24_zwroc_1() {
        Assert.assertEquals(WyszukiwanieLiniowe.znajdzLiczbe(24, array), 1);
    }

    @Test
    public void znajdzLiczbe_10_zwroc_5() {
        Assert.assertEquals(WyszukiwanieLiniowe.znajdzLiczbe(10, array), 5);
    }

    @Test
    public void znajdzLiczbe_4_zwroc_6() {
        Assert.assertEquals(WyszukiwanieLiniowe.znajdzLiczbe(4, array), 6);
    }

    @Test
    public void znajdzLiczbe_21_zwroc_minus1() {
        Assert.assertEquals(WyszukiwanieLiniowe.znajdzLiczbe(21, array), -1);
    }
}