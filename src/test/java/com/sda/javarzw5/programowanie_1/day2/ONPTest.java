package com.sda.javarzw5.programowanie_1.day2;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ONPTest {

    @Test
    public void calculate1() { // 22+=
        Assert.assertEquals(ONP.calculate("22+="), 4, 0);
    }

    @Test
    public void calculate2() { // 73+52-2^*=
        Assert.assertEquals(ONP.calculate("73+52-2^*="), 90, 0);
    }

    @Test
    public void calculate3() { // 62/=
        Assert.assertEquals(ONP.calculate("62/="), 3, 0);
    }

}