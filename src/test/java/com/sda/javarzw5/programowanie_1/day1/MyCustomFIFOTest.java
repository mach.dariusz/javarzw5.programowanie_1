package com.sda.javarzw5.programowanie_1.day1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCustomFIFOTest {

    @Test
    public void push() {
        MyCustomFIFO<String> queue = new MyCustomFIFO<>(3);
        queue.push("A");
        queue.push("B");
        queue.push("C");
        queue.push("D");

        Assert.assertEquals(queue.getSize(), 3);
    }

    @Test
    public void pop() {
        MyCustomFIFO<String> queue = new MyCustomFIFO<>(3);
        queue.push("A");
        queue.push("B");
        queue.push("C");
        Assert.assertEquals(queue.pop(), "A");
        Assert.assertEquals(queue.pop(), "B");
        Assert.assertEquals(queue.pop(), "C");
    }

    @Test
    public void peek() {
        MyCustomFIFO<String> queue = new MyCustomFIFO<>(3);
        queue.push("A");
        queue.push("B");
        Assert.assertEquals(queue.peek(), "A");
    }
}