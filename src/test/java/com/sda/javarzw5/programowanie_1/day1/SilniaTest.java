package com.sda.javarzw5.programowanie_1.day1;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SilniaTest {

    @Test(expected = WrongNumberException.class)
    public void calculate_passedMinusOne_expectedException() {
        Silnia.calculate(-1);
    }

    @Test
    public void calculate_passedZero_expectedOne() {
        Assert.assertEquals(Silnia.calculate(0), 1);
    }

    @Test
    public void calculate_10() {
        Assert.assertEquals(Silnia.calculate(10), 3628800);
    }

    @Test
    public void calculate_3() {
        Assert.assertEquals(Silnia.calculate(3), 6);
    }


}