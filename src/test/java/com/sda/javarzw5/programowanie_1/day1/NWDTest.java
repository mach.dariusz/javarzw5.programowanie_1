package com.sda.javarzw5.programowanie_1.day1;

import org.junit.Assert;
import org.junit.Test;

public class NWDTest {

    @Test()
    public void calculate() {
        Assert.assertEquals(NWD.calculate(2, 2), 2);
        Assert.assertEquals(NWD.calculate(2, 4), 2);
        Assert.assertEquals(NWD.calculate(2, 12), 2);
        Assert.assertEquals(NWD.calculate(3, 9), 3);
        Assert.assertEquals(NWD.calculate(456,  275), 1);
    }
}