package com.sda.javarzw5.programowanie_1.day1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCustomLIFOTest {

    MyCustomLIFO stack;

    @Before
    public void before() {
        this.stack = new MyCustomLIFO(5);
    }

    @Test
    public void push() {

        this.stack.push("Dariusz");
        this.stack.push("Bartosz");
        this.stack.push("Katarzyna");
        this.stack.push("Robert");
        this.stack.push("Paweł");
        this.stack.push("Mirek");

        // this.stack.print();
        Assert.assertEquals(this.stack.getSize(), 5);
    }

    @Test
    public void pop() {
        this.stack.push("darek");
        Assert.assertEquals(this.stack.pop(), "darek");
        this.stack.push("Darek");
        Assert.assertEquals(this.stack.pop(), "Darek");
        this.stack.push("Arek");
        this.stack.push("Bartek");
        Assert.assertEquals(this.stack.pop(), "Bartek");
        this.stack.push("Marek");
        Assert.assertEquals(this.stack.pop(), "Marek");

    }

    @Test
    public void peek() {
        this.stack.push("Darek");
        // System.out.println("peek: " + this.stack.peek());
        Assert.assertEquals(this.stack.peek(), "Darek");
    }
}